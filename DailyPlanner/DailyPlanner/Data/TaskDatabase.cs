﻿using DailyPlanner.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DailyPlanner.Data
{
    public class TaskDatabase
    {
        readonly SQLiteAsyncConnection database;


        public TaskDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<TaskItem>().Wait();
            database.CreateTableAsync<ExternalTask>().Wait();
        }

        //Get all tasks.
        public Task<List<TaskItem>> GetTasksAsync()
        {
            return database.Table<TaskItem>().ToListAsync();
        }

        //Get all tasks due today.
        public Task<List<TaskItem>> GetTodayTasksAsync()
        {
            return database.Table<TaskItem>().Where(i => i.Date == DateTime.Today).ToListAsync();
        }

        //Get all tasks that are not done.
        public Task<List<TaskItem>> GetActiveTasksAsync()
        {
            return database.QueryAsync<TaskItem>("SELECT * FROM [TaskItem] WHERE [Done] = 0");
        }

        // Get a specific task.
        public Task<TaskItem> GetTaskAsync(int id)
        {
            return database.Table<TaskItem>()
                            .Where(i => i.ID == id)
                            .FirstOrDefaultAsync();
        }

        // Save a new task.
        public Task<int> SaveTaskAsync(TaskItem task)
        {
            if (task.ID != 0)
            {
                // Update an existing task.
                return database.UpdateAsync(task);
            }
            else
            {
                return database.InsertAsync(task);
            }
        }

        // Save a new external task.
        public Task<int> SaveExternalTaskAsync(ExternalTask task)
        {
            if (task.ID != 0)
            {
                // Update an existing task.
                return database.UpdateAsync(task);
            }
            else
            {
                return database.InsertAsync(task);
            }
        }

        // Delete a task.
        public Task<int> DeleteTaskAsync(TaskItem task)
        {
            return database.DeleteAsync(task);
        }
    }
}
