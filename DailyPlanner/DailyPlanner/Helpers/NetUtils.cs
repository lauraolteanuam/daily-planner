﻿using DailyPlanner.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DailyPlanner.Helpers
{
    public class NetUtils
    {
        const string url = "https://www.json-generator.com/api/json/get/cfZceborjC?indent=2";


        public async static Task<List<ExternalTask>> GetTasksFromJSON()
        {
            List<TaskItem> tasks = new List<TaskItem>();

            HttpClient httpClient = new HttpClient();
            var resultJson = await httpClient.GetStringAsync(url);

            var resultTasks = JsonConvert.DeserializeObject<List<ExternalTask>>(resultJson);

            foreach (ExternalTask task in resultTasks)
            {
                await App.Database.SaveExternalTaskAsync(task);
            }

            return resultTasks;
        }
    }
}
