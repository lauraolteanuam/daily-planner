﻿using DailyPlanner.Data;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DailyPlanner
{
    public partial class App : Application
    {
        static TaskDatabase database;

        // Create the database connection as a singleton.
        public static TaskDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new TaskDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Tasks.db3"));
                }
                return database;
            }
        }
        public App()
        {
            InitializeComponent();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
