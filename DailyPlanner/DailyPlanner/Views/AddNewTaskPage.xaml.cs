﻿using DailyPlanner.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DailyPlanner.Views
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public partial class AddNewTaskPage : ContentPage
    {
        public string ItemId
        {
            set
            {
                LoadNote(value);
            }
        }

        public AddNewTaskPage()
        {
            InitializeComponent();

            // Set the BindingContext of the page to a new Note.
            BindingContext = new TaskItem();
        }

        async void LoadNote(string itemId)
        {
            try
            {
                int id = Convert.ToInt32(itemId);
                // Retrieve the note and set it as the BindingContext of the page.
                TaskItem task = await App.Database.GetTaskAsync(id);
                BindingContext = task;
            }
            catch (Exception)
            {
                Console.WriteLine("Failed to load task.");
            }
        }

        async void OnSaveButtonClicked(object sender, EventArgs e)
        {
            var task = (TaskItem)BindingContext;
            task.Date = datePicker.Date;
            if (!string.IsNullOrWhiteSpace(task.Title))
            {
                await App.Database.SaveTaskAsync(task);
            }
            

            // Navigate backwards
            await Shell.Current.GoToAsync("..");
        }

        async void OnDeleteButtonClicked(object sender, EventArgs e)
        {
            var task = (TaskItem)BindingContext;
            await App.Database.DeleteTaskAsync(task);

            // Navigate backwards
            await Shell.Current.GoToAsync("..");
        }

    }
}