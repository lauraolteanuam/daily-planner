﻿using DailyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DailyPlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActiveTasksPage : ContentPage
    {
        public ActiveTasksPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            // Retrieve all the tasks that are not done from the database, and set them as the
            // data source for the CollectionView.
            collectionView.ItemsSource = await App.Database.GetActiveTasksAsync();

        }

        async void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.CurrentSelection != null)
            {
                // Navigate to the AddNewTaskPage, passing the ID as a query parameter.
                TaskItem task = (TaskItem)e.CurrentSelection.FirstOrDefault();
                await Shell.Current.GoToAsync($"{nameof(AddNewTaskPage)}?{nameof(AddNewTaskPage.ItemId)}={task.ID.ToString()}");
            }
        }
    }
}