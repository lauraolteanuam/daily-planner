﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DailyPlanner.Models
{
    public class ExternalTask
    {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }

            [JsonProperty("Date")]
            public DateTime Date { get; set; }

            [JsonProperty("Done")]
            public bool Done { get; set; }

            [JsonProperty("Description")]
            public string Description { get; set; }

            [JsonProperty("Title")]
            public string Title { get; set; }
        
    }
}
