﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DailyPlanner.Models
{
    public class TaskItem
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public bool Done { get; set; }
    }
}
